from flask import Flask
from flask_restful import reqparse, abort, Resource, Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

db = SQLAlchemy()
ma = Marshmallow()


class TimeSlot(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    slot1_available = db.Column(db.Boolean, default=False)
    slot2_available = db.Column(db.Boolean, default=False)


class TimeSlotSchema(ma.Schema):
    class Meta:
        fields = ("id", "slot1_available", "slot2_available")


def create_app():
    app = Flask(__name__)
    api = Api(app)

    parser = reqparse.RequestParser()
    parser.add_argument('slot1')
    parser.add_argument('slot2')
    parser.add_argument('slot')
    class TimeSlotAvailable(Resource):
        def get(self, id, slot):
            schema = TimeSlotSchema()
            slots = TimeSlot.query.get(id)
            if not slots:
                abort(404, message="No slots with id:{}".format(id))
            if int(slot) == 1:
                return schema.dump(slots)['slot1_available']
            if int(slot) == 2:
                return schema.dump(slots)['slot2_available']
            else:
                abort(404, message="Only 2 timeslots are available")
        def put(self, id, slot):
            args = parser.parse_args()
            slots = TimeSlot.query.get(id)
            if not slots:
                abort(404, message="No slots with id:{}".format(id))
            if int(slot) == 1:
                slot1 = args['slot']
                if slot1.lower() == "true":
                    slots.slot1_available = True
                else:
                    slots.slot1_available = False
                db.session.commit()
                return 201
            if int(slot) == 2:
                slot2 = args['slot']
                if slot2.lower() == "true":
                    slots.slot2_available = True
                else:
                    slots.slot2_available = False
                db.session.commit()
                return 201
            abort(404, message="Only 2 slots are available id:{}".format(id))


    class SingleTimeSlot(Resource):
        def delete(self, id):
            slots = TimeSlot.query.get(id)
            if not slots:
                abort(404, message="No slots with id:{}".format(id))
            db.session.delete(slots)
            db.session.commit()
            return 201
    class TimeSlots(Resource):
        def post(self):
            args = parser.parse_args()
            slot1 = args['slot1']
            slot2 = args['slot2']
            if slot1.lower() == "true":
                slot1 = True
            else:
                slot1 = False
            if slot2.lower() == "true":
                slot2 = True
            else:
                slot2 = False
            timeslots = TimeSlot(slot1_available=slot1, slot2_available=slot2)
            db.session.add(timeslots)
            db.session.commit()
            return 201

        def get(self):
            cars_schema = TimeSlotSchema(many=True)
            all_slots = TimeSlot.query.all()
            if not all_slots:
                abort(404, message="No timeslots entries")
            return cars_schema.dump(all_slots), 201

    api.add_resource(TimeSlots, '/timelist')
    api.add_resource(TimeSlotAvailable, '/timelist/<id>/<slot>')
    api.add_resource(SingleTimeSlot, '/timelist/<id>')
    return app


def config_db(app):
    app.app_context().push()
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/timeslots.db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    db.create_all()


if __name__ == '__main__':
    app = create_app()
    config_db(app)
    ma.init_app(app)
    app.run(debug=True,port=5003)

