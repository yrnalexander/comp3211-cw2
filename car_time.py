import datetime
from dateutil import parser as dparser
from flask import Flask
from flask_restful import reqparse, abort, Resource, Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow


db = SQLAlchemy()
ma = Marshmallow()


class RentCar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    model = db.Column(db.String(120), unique=False, nullable=False)
    timeslot1 = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now())
    timeslot2 = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now())


class RentSchema(ma.Schema):
    class Meta:
        fields = ("id", "model", "timeslot1", "timeslot2")


def create_app():
    app = Flask(__name__)
    api = Api(app)

    parser = reqparse.RequestParser()
    parser.add_argument('model')
    parser.add_argument('timeslot1')
    parser.add_argument('timeslot2')

    class Car(Resource):
        def get(self, id):
            car_schema = RentSchema()
            car = RentCar.query.get(id)
            if not car:
                abort(404, message="No car with id:{}".format(id))
            return car_schema.dump(car)

        def put(self, id):
            args = parser.parse_args()
            car_schema = RentSchema()
            car = RentCar.query.get(id)
            if not car:
                abort(404, message="No car with id:{}".format(id))
            if args['model']:
                car.model = args['model']
            if args['timeslot1']:
                date_str = args['timeslot1']
                date_parsed = dparser.parse(date_str, ignoretz=True)
                car.timeslot1 = date_parsed
            if args['timeslot2']:
                date_str = args['timeslot2']
                date_parsed = dparser.parse(date_str, ignoretz=True)
                car.timeslot2 = date_parsed

            db.session.commit()
            return 201

        def delete(self, id):
            car_schema = RentSchema()
            car = RentCar.query.get(id)
            if not car:
                abort(404, message="No car with id:{}".format(id))
            db.session.delete(car)
            db.session.commit()
            return 201

    class CarList(Resource):

        def get(self):
            cars_schema = RentSchema(many=True)
            all_cars = RentCar.query.all()
            if not all_cars:
                abort(404, message="No car entries")
            return cars_schema.dump(all_cars), 201

        def post(self):
            args = parser.parse_args()
            date_str1 = args['timeslot1']
            date_parsed1 = dparser.parse(date_str1, ignoretz=True)
            date_str2 = args['timeslot2']
            date_parsed2 = dparser.parse(date_str2, ignoretz=True)
            car = RentCar(model=args['model'], timeslot1=date_parsed1, timeslot2=date_parsed2)
            db.session.add(car)
            db.session.commit()
            return 201

    api.add_resource(Car, '/cars/<id>')
    api.add_resource(CarList, '/cars')
    return app


def config_db(app):
    app.app_context().push()
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/rent.db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    db.create_all()


if __name__ == '__main__':
    app = create_app()
    config_db(app)
    ma.init_app(app)
    if app.config['TESTING'] == True:
        app.run(debug=True, port=5005)
    else:
        app.run(debug=True, port=5002)

