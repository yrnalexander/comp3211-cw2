from flask import Flask, request
from flask_restful import reqparse, abort, Resource, Api

app = Flask(__name__)
api = Api(app)

QUOTES = {
    'quote1': {'text': 'Monday', 'author': 'JP'},
    'quote2': {'text': 'Tuesday', 'author': 'AJ'},
    'quote3': {'text': 'Saturday', 'author': 'EM'}
}


def check_not_existing(quote_id):
    if quote_id not in QUOTES:
        abort(404, message="{} doesn't exist".format(quote_id))


parser = reqparse.RequestParser()
parser.add_argument('text')
parser.add_argument('author')


class Quote(Resource):
    def get(self, quote_id):
        check_not_existing(quote_id)
        return QUOTES[quote_id]

    def put(self, quote_id):
        check_not_existing(quote_id)
        args = parser.parse_args()
        quote_body = {'text': args['text'], 'author': args['author']}
        QUOTES[quote_id] = quote_body
        return quote_body, 201

    def delete(self, quote_id):
        check_not_existing(quote_id)
        del QUOTES[quote_id]
        return '', 204


class QuoteList(Resource):
    def get(self):
        return QUOTES

    def post(self):
        args = parser.parse_args()
        quote_id = int(max(QUOTES.keys()).lstrip('quote')) + 1
        quote_id = 'quote%i' % quote_id
        QUOTES[quote_id] = {'text': args['text'], 'author': args['author']}
        return QUOTES[quote_id], 201


api.add_resource(QuoteList, '/quotes')
api.add_resource(Quote, '/quotes/<quote_id>')

if __name__ == '__main__':
    app.run(debug=True)
