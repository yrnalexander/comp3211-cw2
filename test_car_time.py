import pytest
from dateutil import parser
from car_time import create_app, db, RentCar


def config_db(app):
    app.app_context().push()
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    db.create_all()


@pytest.fixture
def client():
    app = create_app()
    app.config['TESTING'] = True
    config_db(app)
    client = app.test_client()
    yield client
    db.drop_all()


def test_initial(client):
    result = client.get('/cars/1')
    assert "No car with id:1" in str(result.data)

    result = client.get('/cars')
    assert "No car entries" in str(result.data)


def test_post_get(client):
    client.post('http://localhost:5000/cars', data={'model': 'Toyota', 'timeslot1': '20.11.2021 14:00', 'timeslot2': '25.11.2021 14:00'})
    car = RentCar.query.get(1)
    date_parsed = parser.parse('20.11.2021 14:00')
    assert car.model == 'Toyota'
    assert car.timeslot1 == date_parsed

    client.post('http://localhost:5000/cars', data={'model': 'VW', 'timeslot1': '20.12.2021 16:00', 'timeslot2': '25.11.2021 14:00'})
    car = RentCar.query.get(2)
    date_parsed = parser.parse('20.12.2021 16:00')
    assert car.model == 'VW'
    assert car.timeslot1 == date_parsed

    client.post('http://localhost:5000/cars', data={'model': 'Opel', 'timeslot1': '10.12.2021 08:00', 'timeslot2': '25.11.2021 14:00'})
    car = RentCar.query.get(3)
    date_parsed = parser.parse('10.12.2021 08:00')
    assert car.model == 'Opel'
    assert car.timeslot1 == date_parsed

    result = client.get('http://localhost:5000/cars/1')
    assert 'Toyota' in str(result.data)
    assert '2021-11-20T14:00:00' in str(result.data)

    result = client.get('http://localhost:5000/cars/2')
    assert 'VW' in str(result.data)
    assert '2021-12-20T16:00:00' in str(result.data)

    result = client.get('http://localhost:5000/cars/3')
    assert 'Opel' in str(result.data)
    assert '2021-10-12T08:00:00' in str(result.data)


def test_delete(client):
    result = client.get('/cars/1')
    assert 'No car with id:1' in str(result.data)

    client.post('http://localhost:5000/cars', data={'model': 'Toyota', 'timeslot1': '20.11.2021 14:00', 'timeslot2': '25.11.2021 14:00'})
    car = RentCar.query.get(1)
    date_parsed = parser.parse('20.11.2021 14:00')
    assert car.model == 'Toyota'
    assert car.timeslot1 == date_parsed

    client.delete('http://localhost:5000/cars/1')

    result = client.get('/cars/1')
    assert 'No car with id:1' in str(result.data)


def test_put(client):
    client.post('http://localhost:5000/cars', data={'model': 'Toyota', 'timeslot1': '20.11.2021 14:00', 'timeslot2': '25.11.2021 14:00'})
    car = RentCar.query.get(1)
    date_parsed = parser.parse('20.11.2021 14:00')
    assert car.model == 'Toyota'
    assert car.timeslot1 == date_parsed

    client.put('http://localhost:5000/cars/1', data={'model': 'BMW', 'timeslot1': '12.12.2021 15:00', 'timeslot2': '25.11.2021 14:00'})
    result = client.get('/cars/1')
    assert 'BMW' in str(result.data)
    assert '2021-12-12T15:00:00' in str(result.data)

