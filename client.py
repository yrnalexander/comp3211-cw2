from flask import Flask, render_template, redirect, url_for, request
from requests import get, post, put
from dateutil import parser
import json
app = Flask(__name__)


@app.route("/")
def home():
    return redirect(url_for('location'))


@app.route("/location", methods=['POST', 'GET'])
def location():
    if request.method == 'POST':
        form_data = request.form['location']
        output = get('http://localhost:5001/location/{}'.format(form_data))
        obj = json.loads(output.text)
        i = 1
        response = ""
        for el in obj:
            response = response + 'Available car id: ' + str(obj['car{}'.format(i)]['car_id']) + '<br>'
            i = i + 1
        return response
    else:
        return render_template("location.html")


@app.route("/carlist", methods=['POST', 'GET'])
def carlist():
    if request.method == 'POST':
        form_data = request.form['car_id']
        output = get('http://localhost:5002/cars/{}'.format(form_data))
        obj = json.loads(output.text)
        id = obj['id']
        model = obj['model']
        time_str1 = parser.parse(obj['timeslot1']).strftime('"%d %B, %Y"')
        time_str2 = parser.parse(obj['timeslot2']).strftime('"%d %B, %Y"')
        response = 'Car id: {}'.format(id) + '<br>' + 'Car model: {}'.format(model) + '<br>' + 'Timeslot1: {}'.format(time_str1) + '<br>' + 'Timeslot2: {}'.format(time_str2)
        return response
    return render_template("car_select.html")


@app.route("/timeselect", methods=['POST', 'GET'])
def time_select():
    if request.method == 'POST':
        form_id = request.form['select_id']
        timeslot = request.form['time_list']
        output = get('http://localhost:5003/timelist/{}/{}'.format(form_id, timeslot))
        if output.json() is False:
            put('http://localhost:5003/timelist/{}/{}'.format(form_id, timeslot), data={'slot': 'true'})
            return 'Successful reservation'
        else:
            return 'Timeslot is already reserved'
    return render_template("time_select.html")


if __name__ == '__main__':
    app.run(debug=True, port=5000)