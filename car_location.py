from flask import Flask, request
from flask_restful import reqparse, abort, Resource, fields, Api

""" 
API to return a list of cars in the location queried by the client

"""

# API definition for the app
app = Flask(__name__)
api = Api(app)

# List of cars in various
# car_id = id of the car
# location = working location
CARS = {
    'car1' : {
        'car_id' : 1,
        'location' : 'Leeds'},
    'car2' : {
        'car_id' : 2,
        'location' : 'Leeds'},
    'car3' : {
        'car_id' : 3,
        'location' : 'London'}
}

def abort_if_no_car(location):

    # Loops through the cars to check if there is a car in the
    # location
    for car in CARS:
        if CARS[car]['location'].lower() == location.lower():
            return
    
    # Returns a message notifying client that no cars exist in the 
    # given location
    abort(404, message="No cars found in location: {}".format(location))

# Creates a parser for the POST function
car_parser = reqparse.RequestParser()

# Allows the passing in of the location of the car
# in string format
car_parser.add_argument('location')

# GetCar
# Returns a list of all of the cars which share a location with 
# the queried location
class GetCar(Resource):
    
    # Returns a json list of all of the cars that share the 
    # queried location
    def get(self, car_location):

        # Tests if there is a car at the location given before continuing
        abort_if_no_car(car_location)

        # lost to store the cars in the correct location
        car_list = {}

        # int to hold the number of the car returned
        car_number = 1

        # Loops through all of the cars in the CARS list
        for car in CARS:

            # Checks if the location of the car is the same 
            # as the queried location.
            # Basic lowercase check to prevent case mismatch
            if CARS[car]['location'].lower() == car_location.lower():

                # adds the car to the list of cars found
                car_list['car' + str(car_number)] = CARS[car]
                # Iterates the number of cars found
                car_number += 1

        # Returns the full list
        return car_list

# ONLY TO BE USED FOR TESTING
# Allows for return of all cars, the creation of cars and
# the deletion of cars  
class Data(Resource):

    # Returns the full list of cars
    def get(self):
        return CARS
    
    # Creates a Car in the list using data given by the client
    def post(self):

        # Parses the data given by the client
        car_data = car_parser.parse_args()

        # Stores data of the final Car
        last_car = list(CARS.keys())[-1]

        # Creates new car id
        car_id = CARS[last_car]['car_id'] + 1

        # Calculates the name of the new Car
        car = 'car' + str(car_id)

        # Creates the new Car using the client data
        CARS[car] = {'car_id' : car_id,
            'location' : car_data['location'].capitalize()}

        # Returns the newly made Car data
        return CARS[car]

class RemoveData(Resource):

    # Deletes a car using the car_id
    def delete(self, car_id):
        
        # Loops through cars
        for car in CARS:
            # If car_id matches it deletes the car item 
            if CARS[car]['car_id'] == int(car_id):
                del CARS[car]  
              
                # Returns confirmation of the deletion
                return 'Deleted car: ' + car_id
        
        # If no car was found returns an error message
        abort(404, message="No Car with id {} was found".format(car_id))



# Adding API resource routing
api.add_resource(GetCar, '/location/<car_location>')
api.add_resource(Data, '/data')
api.add_resource(RemoveData, '/delete/<car_id>')


if __name__ == '__main__':
    app.run(debug=True, port=5001)